
-- Recreate the Climbers table with auto-increment primary key
CREATE TABLE Climbers (
    ClimberID SERIAL PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    Address VARCHAR(100),
    Email VARCHAR(100),
    Phone VARCHAR(20),
    Gender CHAR(1) NOT NULL CHECK (Gender IN ('M', 'F')),
    record_ts DATE DEFAULT CURRENT_DATE
);


-- Recreate the Countries table with auto-increment primary key
CREATE TABLE Countries (
    CountryID SERIAL PRIMARY KEY,
    CountryName VARCHAR(100) NOT NULL UNIQUE,
    record_ts DATE DEFAULT CURRENT_DATE
);

-- Recreate the Areas table with auto-increment primary key
CREATE TABLE Areas (
    AreaID SERIAL PRIMARY KEY,
    AreaName VARCHAR(100) NOT NULL UNIQUE,
    record_ts DATE DEFAULT CURRENT_DATE
);

-- Recreate the Mountains table with auto-increment primary key
CREATE TABLE Mountains (
    MountainID SERIAL PRIMARY KEY,
    MountainName VARCHAR(100) NOT NULL,
    Height INT CHECK (Height >= 0),
    CountryID INT,
    AreaID INT,
    record_ts DATE DEFAULT CURRENT_DATE,
    FOREIGN KEY (CountryID) REFERENCES Countries(CountryID),
    FOREIGN KEY (AreaID) REFERENCES Areas(AreaID)
);

-- Recreate the Climbs table with auto-increment primary key
CREATE TABLE Climbs (
    ClimbID SERIAL PRIMARY KEY,
    StartDate DATE NOT NULL CHECK (StartDate > '2000-01-01'),
    EndDate DATE NOT NULL CHECK (EndDate > '2000-01-01'),
    MountainID INT,
    LeaderClimberID INT,
    record_ts DATE DEFAULT CURRENT_DATE,
    FOREIGN KEY (MountainID) REFERENCES Mountains(MountainID),
    FOREIGN KEY (LeaderClimberID) REFERENCES Climbers(ClimberID)
);

-- Recreate the Route table with auto-increment primary key
CREATE TABLE Route (
    RouteID SERIAL PRIMARY KEY,
    Length INT CHECK (Length >= 0),
    ClimbID INT,
    record_ts DATE DEFAULT CURRENT_DATE,
    FOREIGN KEY (ClimbID) REFERENCES Climbs(ClimbID)
);

-- Recreate the ClimbingPartners table with auto-increment primary key
CREATE TABLE ClimbingPartners (
    ClimbingPartnerID SERIAL PRIMARY KEY,
    ClimberID1 INT NOT NULL,
    ClimberID2 INT NOT NULL,
    record_ts DATE DEFAULT CURRENT_DATE,
    FOREIGN KEY (ClimberID1) REFERENCES Climbers(ClimberID),
    FOREIGN KEY (ClimberID2) REFERENCES Climbers(ClimberID),
    CHECK (ClimberID1 <> ClimberID2)
);

-- Recreate the ClimbAssistants table with auto-increment primary key
CREATE TABLE ClimbAssistants (
    ClimbAssistantID SERIAL PRIMARY KEY,
    ClimberID INT NOT NULL,
    ClimbID INT NOT NULL,
    record_ts DATE DEFAULT CURRENT_DATE,
    FOREIGN KEY (ClimberID) REFERENCES Climbers(ClimberID),
    FOREIGN KEY (ClimbID) REFERENCES Climbs(ClimbID)
);

-- Recreate the Equipment table with auto-increment primary key
CREATE TABLE Equipment (
    EquipmentID SERIAL PRIMARY KEY,
    EquipmentName VARCHAR(100) NOT NULL UNIQUE,
    Description TEXT,
    record_ts DATE DEFAULT CURRENT_DATE
);

-- Recreate the ClimberEquipment table with auto-increment primary key
CREATE TABLE ClimberEquipment (
    ClimberEquipmentID SERIAL PRIMARY KEY,
    ClimberID INT NOT NULL,
    EquipmentID INT NOT NULL,
    record_ts DATE DEFAULT CURRENT_DATE,
    FOREIGN KEY (ClimberID) REFERENCES Climbers(ClimberID),
    FOREIGN KEY (EquipmentID) REFERENCES Equipment(EquipmentID)
);
