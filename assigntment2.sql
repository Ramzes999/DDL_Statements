-- Insert sample data into Climbs table
INSERT INTO Climbs (StartDate, EndDate, MountainID, LeaderClimberID)
VALUES ('2023-01-01', '2023-01-05', 1, 1),
       ('2023-02-01', '2023-02-10', 2, 2);

-- Insert sample data into Climbers table
INSERT INTO Climbers (FirstName, LastName, Address, Email, Phone, Gender)
VALUES ('John', 'Doe', '123 Main St', 'john@example.com', '123-456-7890', 'M'),
       ('Jane', 'Smith', '456 Oak St', 'jane@example.com', '987-654-3210', 'F');

-- Insert sample data into Mountains table
INSERT INTO Mountains (MountainName, Height, CountryID, AreaID)
VALUES ('Mount Everest', 8848, 1, 1),
       ('K2', 8611, 2, 2);

-- Insert sample data into Countries table
INSERT INTO Countries (CountryName) VALUES ('Nepal'), ('Pakistan');

-- Insert sample data into Areas table
INSERT INTO Areas (AreaName) VALUES ('Himalayas'), ('Karakoram');

-- Insert sample data into Route table
INSERT INTO Route (Length, ClimbID) VALUES (10, 1), (15, 2);

-- Insert sample data into ClimbingPartners table
INSERT INTO ClimbingPartners (ClimberID1, ClimberID2) VALUES (1, 2), (2, 3);

-- Insert sample data into ClimbAssistants table
INSERT INTO ClimbAssistants (ClimberID, ClimbID) VALUES (1, 1), (2, 2);

-- Insert sample data into Equipment table
INSERT INTO Equipment (EquipmentName, Description) VALUES ('Rope', 'Dynamic climbing rope'), ('Harness', 'Climbing harness');

-- Insert sample data into ClimberEquipment table
INSERT INTO ClimberEquipment (ClimberID, EquipmentID) VALUES (1, 1), (1, 2), (2, 2), (3, 1);
